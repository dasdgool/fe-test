import React from "react";
import { HashRouter, Switch, Route } from "react-router-dom";
import Main from "./components/Main/Main";
import ProductsContainer from "./components/Products/ProductsContainer";
import NotFound from "./components/NotFound/NotFound";
import CreateContainer from "./components/CreateOrUpdate/CreateContainer";
import UpdateContainer from "./components/CreateOrUpdate/UpdateContainer";

export function getRoutes() {
  return (
    <HashRouter>
      <Main>
        <Switch>
          <Route exact path="/" component={ProductsContainer} />,
          <Route path="/create" component={CreateContainer} />,
          <Route path="/update/:id" component={UpdateContainer} />,
          <Route path="*" component={NotFound} />,
        </Switch>
      </Main>
    </HashRouter>
  );
}

export default getRoutes;
