import React from "react";
import { connect, useDispatch } from "react-redux";
import PropTypes from "prop-types";
import Product from "./Product";
import { fetchDeleteProduct } from "../../actions/products";

const ProductContainer = ({ product }) => {
  const dispatch = useDispatch();
  const handleProductDelete = () => {
    dispatch(fetchDeleteProduct(product.id));
  };
  return (
    <>
      <Product product={product} onDelete={handleProductDelete} />
    </>
  );
};

ProductContainer.propTypes = {
  product: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
};

export default connect()(ProductContainer);
