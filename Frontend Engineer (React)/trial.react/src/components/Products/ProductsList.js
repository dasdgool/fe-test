import React from "react";
import PropTypes from "prop-types";
import { Container, Row, Col, Button } from "reactstrap";
import { chunk } from "lodash";
import { withRouter } from "react-router-dom";
import ProductContainer from "./ProductContainer";

const ProductList = ({ products, history }) => {
  const productsGroups = chunk(products, 3);

  return (
    <Container>
      <Button
        onClick={() => history.push("/create")}
        style={{ marginBottom: "16px" }}
        block
        outline
        color="primary"
      >
        Create
      </Button>
      {productsGroups.map((productsGroup, index) => (
        <Row key={index} className="mb-5">
          {productsGroup.map((product) => (
            <Col sm="4" key={`product_${product.id}`}>
              <ProductContainer product={product} />
            </Col>
          ))}
        </Row>
      ))}
    </Container>
  );
};

ProductList.propTypes = {
  products: PropTypes.array.isRequired,
};

export default withRouter(ProductList);
