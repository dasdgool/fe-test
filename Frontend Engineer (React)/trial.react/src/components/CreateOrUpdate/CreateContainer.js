import React, { useEffect } from "react";
import { connect, useDispatch } from "react-redux";
import PropTypes from "prop-types";
import { fetchCategories } from "../../actions/categories";
import CreateOrUpdate from "./CreateOrUpdate";
import { fetchAddProduct } from "../../actions/products";

const CreateContainer = ({ categories, history }) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchCategories());
  }, [dispatch]);

  const handleCreateProduct = (json) => {
    dispatch(fetchAddProduct(json));
    history.push("/");
  };

  return (
    <div>
      <CreateOrUpdate
        categoriesArray={categories}
        onClick={handleCreateProduct}
      />
    </div>
  );
};

CreateContainer.propTypes = {
  categories: PropTypes.array.isRequired,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  categories: state.categories,
});

export default connect(mapStateToProps)(CreateContainer);
