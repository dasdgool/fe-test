import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
  Card,
  CardText,
  CardBody,
  FormGroup,
  Button,
  CardHeader,
  CardFooter,
  Form,
  Input,
  Label,
  Toast,
  ToastBody,
} from "reactstrap";
import moment from "moment";
import MultiSelect from "../MultiSelect/MultiSelect";
import { withRouter } from "react-router-dom";

const CreateOrUpdate = ({ product, categoriesArray, onClick, history }) => {
  const id = product ? product.id : null;
  const [formState, setFormState] = useState({
    brand: "",
    categories: [1],
    createdAt: new Date().toString(),
    expirationDate: null,
    featured: false,
    itemsInStock: 0,
    name: "",
    rating: 1,
    receiptDate: null,
  });

  useEffect(() => {
    setFormState({
      brand: product ? product.brand : "",
      categories: product ? product.categories : [1],
      createdAt: product ? product.createdAt : new Date().toString(),
      expirationDate: product ? product.expirationDate : null,
      featured: product ? product.featured : false,
      itemsInStock: product ? product.itemsInStock : 0,
      name: product ? product.name : "",
      rating: product ? product.rating : 1,
      receiptDate: product ? product.receiptDate : null,
    });
  }, [product]);

  const [hasExpirationDate, setExpirationDate] = useState(false);

  const [error, setError] = useState(false);

  const handleFormInputs = (type, value) => {
    setFormState({ ...formState, [type]: value });
  };

  const minExpirationDate = moment().add(30, "d").format("YYYY-MM-DD");

  const handleClick = () => {
    const form = createForm();
    if (!form.name) {
      setError(true);
      setTimeout(() => setError(false), 2000);
    } else {
      onClick(form);
    }
  };

  const createForm = () => {
    const form = {
      brand: formState.brand || "",
      categories: Object.values(
        formState.categories.map((category) => category + 1)
      ),
      createdAt: moment().format(),
      expirationDate: formState.expirationDate
        ? moment(formState.expirationDate).format("yyyy-MM-DD")
        : null,
      featured: formState.rating >= 8,
      itemsInStock: formState.itemsInStock || 0,
      name: formState.name,
      rating: formState.rating,
      receiptDate: formState.receiptDate
        ? moment(formState.receiptDate).format("yyyy-MM-DD")
        : null,
    };

    if (id) form.id = id;

    return form;
  };

  const title = id ? "Update" : "Create";

  return (
    <Card>
      {error && (
        <Toast className="bg-danger">
          <ToastBody>Name is required!</ToastBody>
        </Toast>
      )}

      <CardHeader>{title} Product</CardHeader>
      <CardBody>
        <CardText tag="div">
          <Form>
            <FormGroup>
              Name:
              <Input
                required
                maxLength={200}
                value={formState.name}
                onChange={(e) => handleFormInputs("name", e.target.value)}
              ></Input>
            </FormGroup>
            <FormGroup>
              Brand:
              <Input
                value={formState.brand}
                onChange={(e) => handleFormInputs("brand", e.target.value)}
              ></Input>
            </FormGroup>
            <FormGroup>
              Items in Stock:
              <Input
                value={formState.itemsInStock}
                type="number"
                onChange={(e) =>
                  handleFormInputs("itemsInStock", e.target.value)
                }
              ></Input>
            </FormGroup>
            <FormGroup>
              Rating:
              <Input
                value={formState.rating}
                type="select"
                onChange={(e) => handleFormInputs("rating", e.target.value)}
              >
                {Array.apply(null, { length: 10 })
                  .map(Number.call, Number)
                  .map((val) => (
                    <option key={`rating_${val}`} value={val + 1}>
                      {val + 1}
                    </option>
                  ))}
              </Input>
            </FormGroup>
            <FormGroup>
              <Label>Categories</Label>
              <MultiSelect
                length={5}
                value={formState.categories}
                items={categoriesArray}
                onChange={(value) => handleFormInputs("categories", value)}
              />
            </FormGroup>
            <FormGroup check>
              <Label check>
                <Input
                  value={!!formState.expirationDate}
                  onChange={() => setExpirationDate(!hasExpirationDate)}
                  type="checkbox"
                ></Input>
                {"has expiration date"}
              </Label>
            </FormGroup>
            {hasExpirationDate && (
              <FormGroup>
                <Label>Expiration Date:</Label>
                <Input
                  value={moment(formState.expirationDate).format("yyyy-MM-DD")}
                  type="date"
                  min={minExpirationDate}
                  onChange={(e) =>
                    handleFormInputs("expirationDate", e.target.value)
                  }
                />
              </FormGroup>
            )}
            <FormGroup>
              <Label>Receipt Date:</Label>
              <Input
                value={moment(formState.receiptDate).format("yyyy-MM-DD")}
                type="date"
                onChange={(e) =>
                  handleFormInputs("receiptDate", e.target.value)
                }
              />
            </FormGroup>
          </Form>
        </CardText>
      </CardBody>
      <CardFooter>
        <Button outline color="primary" onClick={handleClick}>
          {title}
        </Button>{" "}
        <Button onClick={() => history.goBack()} outline>
          Cancel
        </Button>
        {id && (
          <Button style={{ float: "right" }} color="danger" outline>
            Delete
          </Button>
        )}
      </CardFooter>
    </Card>
  );
};

CreateOrUpdate.propTypes = {
  categoriesArray: PropTypes.array.isRequired,
};

export default withRouter(CreateOrUpdate);
