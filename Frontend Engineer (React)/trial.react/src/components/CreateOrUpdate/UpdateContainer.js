import React, { useEffect, useState } from "react";
import { connect, useDispatch } from "react-redux";
import PropTypes from "prop-types";
import { fetchCategories } from "../../actions/categories";
import CreateOrUpdate from "./CreateOrUpdate";
import { fetchProducts, fetchUpdateProduct } from "../../actions/products";
import { useParams } from "react-router-dom";

const UpdateContainer = ({ categories, products, history }) => {
  const [product, setProduct] = useState({});
  const dispatch = useDispatch();

  const { id } = useParams();

  useEffect(() => {
    dispatch(fetchCategories());
    dispatch(fetchProducts());
    const currentProduct = products.find((val) => val.id.toString() === id);
    setProduct(currentProduct);
  }, [dispatch, id]);

  const handleUpdateProduct = (json) => {
    dispatch(fetchUpdateProduct(json));
    history.push("/");
  };

  return (
    <div>
      {product && (
        <CreateOrUpdate
          id={id}
          product={product}
          categoriesArray={categories}
          onClick={handleUpdateProduct}
        />
      )}
    </div>
  );
};

UpdateContainer.propTypes = {
  categories: PropTypes.array.isRequired,
  products: PropTypes.array.isRequired,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  categories: state.categories,
  products: state.products,
});

export default connect(mapStateToProps)(UpdateContainer);
