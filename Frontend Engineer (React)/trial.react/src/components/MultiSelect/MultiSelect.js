import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Input, Button, FormGroup } from "reactstrap";

const MultiSelect = ({ items, length, onChange, value }) => {
  const [currentItems, setCurrentItems] = useState([]);

  const [filteredSelects, setFilteredSelect] = useState([]);

  useEffect(() => {
    const values = value || [];
    const newFiltered = [items];
    values.forEach((elem, index) => {
      if (index > 0) {
        newFiltered.push(
          newFiltered[index - 1].filter((item) => values[index - 1] !== item.id)
        );
      }
      if (elem > index) elem--;
      if (elem < index) elem++;
    });
    setFilteredSelect(newFiltered);
    setCurrentItems(values);
  }, [items, value]);

  const handleSelect = () => {
    onChange(currentItems);
  };

  const handleChangeSelect = (e) => {
    const newCurrent = [...currentItems];
    newCurrent[e.target.id] = parseInt(e.target.value);
    setCurrentItems(newCurrent);
    handleSelect();
  };

  const handleAddButton = () => {
    const newArray = items.filter(
      (item) => !Object.values(currentItems).some((curr) => curr === item.id)
    );
    const filtered = [...filteredSelects, newArray];
    setFilteredSelect(filtered);
    const newCurrent = [...currentItems];
    newCurrent[filtered.length - 1] = newArray[0].id;
    setCurrentItems(newCurrent);
    handleSelect();
  };

  return (
    <FormGroup>
      {filteredSelects.map((filteredItems, index) => (
        <Input
          value={currentItems[index]}
          onChange={(e) => handleChangeSelect(e)}
          type="select"
          key={`select_${index}`}
          id={`select_${index}`}
        >
          {filteredItems.map((item) => (
            <option key={`select_${index}__${item.id}`} value={item.id}>
              {item.name}
            </option>
          ))}
        </Input>
      ))}
      {filteredSelects.length < length && (
        <div style={{ paddingTop: "8px" }}>
          <Button outline block color="primary" onClick={handleAddButton}>
            add
          </Button>
        </div>
      )}
    </FormGroup>
  );
};

MultiSelect.propTypes = {
  items: PropTypes.array.isRequired,
};

export default MultiSelect;
