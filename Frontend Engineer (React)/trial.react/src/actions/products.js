import { productApi } from "../gateways/ProductApi";

export const REQUEST_PRODUCTS = "REQUEST_PRODUCTS";
export const RECEIVE_PRODUCTS = "RECEIVE_PRODUCTS";
export const CREATE_PRODUCT = "CREATE_PRODUCT";
export const UPDATE_PRODUCT = "UPDATE_PRODUCT";
export const DELETE_PRODUCT = "DELETE_PRODUCT";

const requestProducts = () => ({
  type: REQUEST_PRODUCTS,
});

const receiveProducts = (json) => ({
  type: RECEIVE_PRODUCTS,
  products: json.map((product) => product),
});

export const fetchProducts = () => (dispatch) => {
  dispatch(requestProducts());
  const json = productApi.getProducts();
  dispatch(receiveProducts(json));
};

const createProducts = (json) => ({
  type: CREATE_PRODUCT,
  product: json,
});

export const fetchAddProduct = (json) => (dispatch) => {
  const products = productApi.getProducts();
  const id = products.length + 1;
  productApi.setProducts([...products, { ...json, id }]);
  dispatch(createProducts(json));
  console.log([...products, { ...json, id }]);
};

const updateProduct = (json) => ({
  type: UPDATE_PRODUCT,
  product: json,
});

export const fetchUpdateProduct = (json) => (dispatch) => {
  const products = productApi.getProducts();
  products[products.findIndex((product) => product.id === json.id)] = json;
  productApi.setProducts([products]);
  dispatch(updateProduct(json));
};

const deleteProduct = (id) => ({
  type: DELETE_PRODUCT,
  id: id,
});

export const fetchDeleteProduct = (id) => (dispatch) => {
  const products = productApi
    .getProducts()
    .filter((product) => product.id !== id);
  productApi.setProducts([products]);
  dispatch(deleteProduct(id));
};
