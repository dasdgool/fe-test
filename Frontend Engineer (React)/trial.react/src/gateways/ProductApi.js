import products from "../mocks/products";

class ProductApi {
  _products = products;
  getProducts = () => {
    return this._products;
  };
  setProducts = (value) => {
    this._products = value;
  };
}

export const productApi = new ProductApi();
