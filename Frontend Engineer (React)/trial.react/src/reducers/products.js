import * as productsActions from "../actions/products";

export function products(state = [], action) {
  switch (action.type) {
    case productsActions.RECEIVE_PRODUCTS:
      return [...action.products];
    case productsActions.CREATE_PRODUCT:
      return [...state, action.product];
    case productsActions.UPDATE_PRODUCT:
      return [...state, action.product];
    case productsActions.DELETE_PRODUCT:
      const products = state.filter((product) => product.id !== action.id);
      return [...products];
    default:
      return state;
  }
}
